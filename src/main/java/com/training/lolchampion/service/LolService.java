package com.training.lolchampion.service;

import com.training.lolchampion.model.LolChampion;
import com.training.lolchampion.repository.LolChampionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LolService {

    @Autowired
    private LolChampionRepository lolChampionRepository;

    public List<LolChampion> findAll() {
        return lolChampionRepository.findAll();
    }

    public LolChampion save(LolChampion champion) {
        return lolChampionRepository.save(champion);
    }
}
