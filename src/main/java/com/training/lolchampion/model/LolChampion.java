package com.training.lolchampion.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@ToString
@Document
public class LolChampion {
    @Id
    private String id;
    private String name;
    private String role;
    private String difficulty;
}
