package com.training.lolchampion.controller;

import com.training.lolchampion.model.LolChampion;
import com.training.lolchampion.service.LolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
public class LolController {

    @Autowired
    private LolService lolService;

    @GetMapping
    public List<LolChampion> findAll() {
        return lolService.findAll();
    }

    @PostMapping
    public LolChampion save(@RequestBody LolChampion lolChampion) {
        return lolService.save(lolChampion);
    }
}
