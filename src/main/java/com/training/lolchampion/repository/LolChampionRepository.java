package com.training.lolchampion.repository;

import com.training.lolchampion.model.LolChampion;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LolChampionRepository extends MongoRepository<LolChampion, String> {
}
