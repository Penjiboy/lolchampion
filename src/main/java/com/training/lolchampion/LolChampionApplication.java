package com.training.lolchampion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LolChampionApplication {

    public static void main(String[] args) {
        SpringApplication.run(LolChampionApplication.class, args);
    }

}
